geometricopticssimulation |version|
===================================

*geometricopticssimulation* provides simulation for geometric optics.

*geometricopticssimulation* has been developed at the `Department of Physics <https://www.fysik.dtu.dk/>`_
of `Technical University of Denmark <https://www.dtu.dk/>`_ and is maintained by the `Software group <http://www.esrf.eu/Instrumentation/software>`_
of the `European Synchrotron <https://www.esrf.eu/>`_.

Documentation
-------------

.. toctree::
    :maxdepth: 2

    api
