# geometricopticssimulation

Simulation for geometric optics.

## Run tests

From pypi (not released yet)

```bash
pip install geometricopticssimulation[test]

pytest --pyargs geometricopticssimulation.tests
```

From source

```bash
pip install -e .[test]

pytest .
```

## Code style

Run auto formatting on the repository

```bash
black .
```

Check for PEP8 compliance

```bash
flake8
```

## Documentation

https://geometricopticssimulation.readthedocs.io/
